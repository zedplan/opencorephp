<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */


/**
 * Make a string suitable for URL's. Replace spaces by dashes and remove any non-alphanumeric characters.
 * Use Strings#getSlug instead
 *
 * @param string $input
 * @return string
 * @author ZedPlan Team (opencorephp@zedplan.com)
 * @deprecated
 * @see Strings#getSlug
 */
function str2url($input) {
	$input = str_replace(array('á','é','í','ó','ú'), array('a','e','i','o','u'), mb_strtolower($input));
	$input = preg_replace(array('#\s+#', '#[^\w-]#', '#-+#'), array('-', '', '-'), $input);
	return $input;
}
?>