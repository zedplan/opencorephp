<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */


//namespace gui;

/**
 * This class renders a view.
 * A view can include any number of templates.
 * 
 * @package gui
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
class View
{
	/**
	 * @var Request
	 */
	protected $_request;
	/**
	 * @var Config
	 */
	protected $_config;
	/**
	 * Array with template names.
	 * @var string[]
	 */
	protected $_templates;
	/**
	 * Array with template data.
	 * @var mixed[]
	 */
	protected $_data;
	/**
	 * Global data.
	 * @var mixed[]
	 * @static
	 */
	protected static $_globalData = array();
	
        protected $_viewsDir;
        
        /**
         * @var string[]
         */
        protected $_autoDetectedFilesPath = array();
        
	/**
	 * Render template(s).
	 * Variable precedence is as follows:
	 * - Local data
	 * - View data
	 * - Global data
	 * 
	 * @param string|string[] $templateName A single template or an array of templates.
	 * @param mixed[] $data Array with data. Its values will be extracted and converted into local variables inside the template.
	 * @return string
	 */
	protected function _renderTemplate($templateName, array $data = array())
	{
		$__templateName____ = (array)$templateName;
		$__data____ = array_merge(self::$_globalData, $this->_data, $data);
		unset($templateName, $data);
		extract($__data____, EXTR_SKIP);
		ob_start();
		$__printTplName____ = $this->_config->get('views.print_tpl_name');
		
		foreach ($__templateName____ as $__tpl____) {
			$__file____ = $this->getViewsDir() . DIRECTORY_SEPARATOR . $__tpl____
						. $this->_config->get('views.file_extension');
			if ($__printTplName____) echo "<!-- Begin Template: $__tpl____ -->\n";
			include($__file____);
			if ($__printTplName____) echo "\n<!-- End Template: $__tpl____ -->";
			echo "\n";
		}
		
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	/**
	 * Check if template exists.
	 *
	 * @param string $template
	 * @return void
	 * @throws FileNotFoundException if template doesn't exist.
	 */
	protected function _checkTemplate(&$template)
	{       
                
                if( strpos( $template, '@') === 0 ){
                    $template = $this->getAutoDetectedFilesPath().'/'.substr($template, 1);
                }
            
		$config = Config::getInstance();
		$file =  $this->getViewsDir() . DIRECTORY_SEPARATOR . $template . $config->get('views.file_extension');
		if (!file_exists($file)) {
			import("io.FileNotFoundException");
			throw new FileNotFoundException("Invalid template: $template. File '$file' not found.");
		}
	}
	
	/**
	 * Check if template exists.
	 *
	 * @param string $template
	 * @return void
	 * @throws FileNotFoundException if template doesn't exist.
	 */
	protected static function __checkTemplate($template)
	{
		$config = Config::getInstance();
		$file =  $config->get('views.dir') . DIRECTORY_SEPARATOR . $template . $config->get('views.file_extension');
		if (!file_exists($file)) {
			import("io.FileNotFoundException");
			throw new FileNotFoundException("Invalid template: $template. File '$file' not found.");
		}
	}
	
    public function setViewsDir($folder){
        
        $this->_viewsDir = $folder;
    }
	
    protected function getViewsDir(){
        
        $config = $this->_config; //die((!empty($this->_viewsDir)) ? $this->_viewsDir : $config->get('views.dir'));
        return (!empty($this->_viewsDir)) ? $this->_viewsDir : $config->get('views.dir'); 
    }
        
    /**
    * Create vars in the given view
    *
    * @param View  &$view 
    * @param array $vars
    */
    public function createVars($vars)
    {
            foreach ($vars as $field=>$value)
                $this->$field = $value;
        }
        
	/**
	 * Set global variable(s) for later usage inside the templates.
	 * If an array is provided, its keys are extracted and converted into variable names inside the template.
	 *
	 * @param string|mixed[] $name A single variable name or an array of names and values.
	 * @param string $value
	 * @return void
	 */
	public static function setGlobal($name, $value = "")
	{
		if (is_array($name)) {
			self::$_globalData = array_merge(self::$_globalData, $name);
		}
		else {
			self::$_globalData[$name] = $value;
		}
	}
	
	/**
	 * Constructor. Creates a view that will render the specified templates.
	 *
	 * @param string|string[] $template A single template name or an array of templates.
	 * @param mixed[] $data Data array where keys are valid variable names. They will be extracted and converted into local variables for each template.
     * @param Views dir if the one in config/views.php is not wanted
	 * @throws FileNotFoundException if $template does not represent an existent template file.
	 */
	public function __construct(/*$template = null, array $data = array()*/)
	{
            $args = func_get_args();
            $template = $args[0];
            
            $this->setAutoDetectedFilesPath();
            
            $data = (isset($args[1]) && is_array($args[1])) ? $args[1] : array();

            $viewsDir = (empty($args[2]) && isset($args[1]) && is_string($args[1])) ? $args[1] : @$args[2]; // default will be retrieved by getViewsDir() method

            $this->_data = $data;
            $this->_config = Config::getInstance(); 
            $this->_request = Request::getInstance();
            $this->_viewsDir = $viewsDir;
            
            
            $this->_templates = (array)$template;
            foreach ($this->_templates as &$tpl) {
                    self::_checkTemplate($tpl);
            }
		
	}
	
	/**
	 * Get a DocumentView instance that includes this view's templates and data.
	 *
	 * @param string $title Document title
	 * @return DocumentView
	 */
	public function getDocument($title = null)
	{
		import('gui.DocumentView');
		return new DocumentView($this->_templates, $title, $this->_data);
	}

	/**
	 * Add template(s).
	 *
	 * @param string|string[] $template A single template or an array of templates.
	 * @return void
	 */
	public function addTemplate($template)
	{
		$templates = (array)$template;
		foreach ($templates as $tpl) {
			self::_checkTemplate($tpl);
		}
		$this->_templates = array_merge($this->_templates, $templates);
	}
	
	/**
	 * Get templates
	 * @return string[]
	 */
	public function getTemplates()
	{
		return $this->_templates;
	}
	
        public function getAutoDetectedFilesPath() {
            return $this->_autoDetectedFilesPath;
        }
        
	/**
	 * Set variable(s) for later usage inside the templates.
	 * If an array is provided, its keys are extracted and converted into variable names inside the template.
	 * Local variables replaces global variables.
	 *
	 * @param string|mixed[] $name A single variable name or an array of names and values.
	 * @param string $value
	 * @return void
	 */
	public function set($name, $value = "")
	{
		if (is_array($name)) {
			$this->_data = array_merge($this->_data, $name);
		}
		else {
			$this->_data[$name] = $value;
		}
	}
        
      /**
        * Autodetect files (for js, css, views) path based on controllers path
        */
        public function setAutoDetectedFilesPath(){

           try {
               $traces = debug_backtrace();

               $lastTrace = 0;
               foreach( $traces as $i => $trace ){
                   if( empty( $trace['file']) ){
                       $lastTrace = --$i; // get the last trace with file, this is the file from where the DocumentView() was instanciated.
                   }

                   if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
                       if(strpos($trace['file'], '/packages/core/Router.php') !== false){
                           // get the trace where use class DocumentView and have args route
                           $lastTrace = --$i;
                           break;
                       }
                   }
               }

               $pathinfo = pathinfo( $traces[$lastTrace]['file'] );
               $dirName = $pathinfo['dirname'];
               $fileName = $pathinfo['filename'];

               $dirNameStartPos = strpos( $dirName, 'controllers/' ) + strlen( 'controllers/' );
               $fileNameStartPos = strpos( $fileName, 'Controller' );

               $path =  substr( $dirName, $dirNameStartPos ) .'/'. strtolower( substr( $fileName , 0, $fileNameStartPos ) );

               $this->_autoDetectedFilesPath = $path;

           } catch (Exception $e) {
               fb('No se pudo autodetectar la ruta.');
               $this->_autoDetectedFilesPath = false;
           }

        }
        
	/**
	 * Renders the registered templates.
	 *
	 * @param boolean|callback $filter If a callback is provided it will be called with the rendered html source as the first parameter. If TRUE, new lines and tabs will be removed.
	 * @return string
	 */
	public function render($filter = false)
	{
		$html = '';
		if (!empty($this->_templates)) {
			$html .= $this->_renderTemplate($this->_templates);
		}
		if (is_callable($filter)) $html = call_user_func($filter, $html);
		else if ($filter === true) $html = str_replace(array("\n", "\t", "\r"), '', $html);
		return $html;
	}

	/**
	 * Get template variables.
	 * 
	 * @return mixed[]
	 */
	public function getData()
	{
		return $this->_data;
	}
	
	/**
	 * Magic mathod to dinamically set template data.
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public function __set($name, $value)
	{
		$this->_data[$name] = $value;
	}
	
	/**
	 * Magic method to fetch a variable previously assigned.
	 * If variable does not exist, an empty string is returned.
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		return isset($this->_data[$name]) ? $this->_data[$name] : '';
	}
	
	/**
	 * Returns view's HTML source.
	 * Note that exceptions thrown within any template will be caught and sent to the registered exception handler.
	 *
	 * @return string
	 */
	public function __toString()
	{
		try {
			$source = $this->render();
		} catch (Exception $ex) {
			$config = Config::getInstance();
			try {
				if (!($ex instanceof LoggerException) && $config->get('logs.log_exceptions')) {
					import('log.Logger');
					Logger::getInstance()->log($ex, Logger::TYPE_EXCEPTION);
				}
			} catch (Exception $ex2) { }
			$call = $config->get('core.exception_handler');
			if ($call && is_callable($call)) {
				call_user_func($call, $ex);
			}
			$source = "";
		}
		return $source;
	}
}
?>