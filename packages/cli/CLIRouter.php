<?PHP
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2014, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */

require_once (FRAMEWORK_DIR. '/functions/camel.php');

import('core.Config');

class CLIRouter {

    private static $instance = null;

    private $_options = null;
    private $_dispatched = false;
    private $_scriptPath = null;

    static function getInstance () {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }

    public static function throwException(Exception $ex, $debug = null) {

        if (is_null($debug)) {
            $debug = DEBUG_MODE;
        }

        $msg  = "Script finish with an error: ";
        $msg .= $ex->getMessage();
        $msg .= "\n";
        $code = 1;

        if ($ex->getCode() && is_integer($ex->getCode()))
            $code = $ex->code;

        if ($debug) {

            $file = file_exists($ex->getFile()) ?
                file($ex->getFile()) :
                null;

            $msg .= "\nDebug info:\n";
            $msg .= $ex->getTraceAsString() . "\n\n";
            $msg .= "{$ex->getFile()} on {$ex->getLine()}:\n";

            if ($file && $ex->getLine()) {
                $fline = $ex->getLine() - 5 >= 1 ? $ex->getLine() -5 : 1;
                $tline = $ex->getLine() + 5 <= count($file) ? 
                    $ex->getLine() + 5 : count($file);

                for ($i=$fline; $i <= $tline; $i++) {
                    $m = $i == $ex->getLine() ? ">>" : "$i";
                    $msg .= "{$m}\t {$file[$i-1]}";
                }

                $msg .="\n";

            }

        }

        fwrite(STDERR, $msg);
        exit($code);

    }

    protected function __construct($path = null) {
        if (is_null($path))
            $path = Config::getInstance()->get("core.cli-scripts.dir");
        $this->_scriptPath = $path;
    }

    private function _parseCliOptions ($cliop) {

        if (isset($cliop['m'])) $cliop['module'] = $cliop['m'];
        if (isset($cliop['t'])) $cliop['task'] = $cliop['t'];
        if (isset($cliop['d'])) $cliop['debug'] = $cliop['d'];

        $this->_options = new DataInput($cliop);

        $this->_options->init('module', 'string','main');
        $this->_options->init('task', 'string', 'default');
        $this->_options->init('debug', 'boolean', false);
   
    }

    private function _getPath ($controller) {
        $filename = preg_replace('#(^[\w]+)#','', $controller);
        $filename = to_camel_case($controller,true);
        $path = $this->_scriptPath . DIRECTORY_SEPARATOR . "{$filename}.php";
        return $path;
    } 

    public function dispatch($cliop) {

        $this->_parseCliOptions($cliop);

		if ($this->_dispatched) return;
		
		$this->_dispatched = true;

		try {

            $controller = $this->_options->getValue('module');
            $task = $this->_options->getValue('task');

            $controllerFilePath = $this->_getPath($controller); 
            $controllerClass = to_camel_case($controller, true) . "Script";

            $taskMethod = to_camel_case($task) . "Action";

            if (!file_exists($controllerFilePath)) {
                throw new Exception("Does not exists script in $controllerFilePath");
            }

			require_once($controllerFilePath);
			
			if (!class_exists($controllerClass)) {
				throw new ClassDefNotFoundException("Class '{$controllerClass}' not found.");
			}

            $controller = new $controllerClass(getopt($cliparams, $largeCliparams));

			// make sure we have an instance of Controller class
			if (!$controller instanceof CLIController) {
				throw new RuntimeException("Class '{$controllerClass}' does not extends from 'CLIController' class.");
			}

            $cliparams = "";
            $largeCliparams = Array();

            if (property_exists($controllerClass, '_cliparams')) {
                $cliparams = $controllerClass::_cliparams; 
            }

            if (property_exists($controllerClass, '_largecliparams')) {
                $largeCliparams = $controllerClass::_largecliparams; 
            }
            
            if (!method_exists($controller,$taskMethod)) {
                throw new Exception ("Module $module has not have task $task (method $taskMethod)");
            }

            $code = call_user_func(Array($controller, $taskMethod), $cliop );

            if ($code && is_integer($code)) {
                exit($code);
            } else {
                exit(0);
            }

        } catch (Exception $ex) {
            $debug = $this->_options->getValue('debug') ? true: null;
            self::throwException($ex, $debug);
		}
	}

}
