<?php

/**
 * Description of SQLSingleResult
 *
 * @author guille
 * @ version 0.1 beta
 */
class SQLResultsCollection {
      
//    private $prefix = null; 
    protected $fields = array(); 
    
    function __construct(){
        
//        try {
//                        
//            $args = func_get_args();
//            if (!empty($args) && !empty($args[0])) {
//                $this->setMainField($args[0]); // this should be a primary key 
//            }
//            
//        } catch( Exception $exc ){
//            fb($exc);
//        }
    }
    
     /**
     * Get field From Result
     * @param type $fieldName
     * @return type
     */
    public function getField($fieldName) {
     
        $column = /*(!empty($this->prefix)) ? $this->prefix."_".from_camel_case($fieldName) :*/ from_camel_case($fieldName);
        return ireturn($this->fields[$column]);
    }
    
    /**
     * Gets result data as array
     * @return type
     */
    public function getData(){
        
        return ireturn($this->fields);
    }
  
    function isEmpty(){
        
        foreach ($this->fields as $value) {
            if($value)
                return false;
        }
        return true;
        
    }
   
    function save(){
        
        // TODO: instanciar modelo, llenarlo y llamar a save()
    }
  
    function delete(){
        
        // TODO: instanciar modelo, llenarlo y llamar a delete()
    } 
}

?>
