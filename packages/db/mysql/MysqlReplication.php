<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2013, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2013, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 * @
 */

// namespace db\mysql;

import("db.BaseSQL");

Class MysqlReplication extends BaseSQL{
    
    public static function getShowMasterStatus($conn=''){
        
        $sql="SHOW MASTER STATUS;";
        
        if ($rst= self::doIQuery($sql, true, $conn))
            return $rst;
        else {
            throw new Exception("This MySQL Server is not running as MASTER.");
        }
     
    }
    
    public static function getShowSlaveStatus($conn=''){
        
        $sql="SHOW SLAVE STATUS;";
        
        if ($rst= self::doIQuery($sql, true, $conn))
            return $rst;
        else {
            throw new Exception("This MySQL Server is not running as SLAVE.");
        }
     
    }
    
    
    
    
    
}
?>
