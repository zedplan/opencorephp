<?php

/**
 * Description of tables
 *
 * @author mike
 */
class Tables extends BaseSQL {


    static function getTablesList(){
        $sql ="SHOW TABLES;";
        return (array) self::doIQuery($sql);
    }
    
    static function getColumnsList($table_name, $full = TRUE){
        if (strlen($table_name)<1) return FALSE;
        $sql ="SHOW ". ($full?'FULL ':'') ."COLUMNS FROM $table_name;";
        return self::doIQuery($sql);
    }
    
    static function getColumns($table_name){
        if (strlen($table_name)<1) return FALSE;
        $sql ="SHOW COLUMNS FROM $table_name;";
            
        if (!$rst= self::doIQuery($sql)){
            return false;
        }else {
            foreach ($rst as &$field) {
                $field= $field['Field'];
            }
            return $rst;
        }
    }
    
    static function getTableIndex($table_name){
        if (strlen($table_name)<1) return FALSE;
        $sql ="SHOW INDEX FROM $table_name;";
        return self::doIQuery($sql);
    }
    
    static function getTablePk($table_name){
         
        if (strlen($table_name)<1) return FALSE;
        $sql ="SHOW INDEX FROM $table_name WHERE Key_name = 'PRIMARY';"; 
       
        $result = BaseSQL::doIQuery($sql, true);
     
        return ireturn($result['Column_name']);
    }    
}

?>
