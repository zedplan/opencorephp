<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */

//namespace core;

/**
 * This class have a validation class using GUMP.
 * doc: https://github.com/Wixel/GUMP
 *
 * @package core
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
class RequestValidator
{
    protected $rules;

    public function __construct(array $rules = [])
    {
        $this->rules = (array) $rules;
    }

    /**
     * Return array with rules
     * @return array
     */
    public function rules(){
        return $this->rules;
    }

    public function failed(array $errors = []){
        throw new Exception("Validation failed: \r\n-". implode("\r\n-", $errors));
    }

    /**
     * @return bool
     */
    public function authorization(){
        return true;
    }

    /**
     * return instance of self
     * @return static
     */
    public static function createInstance(){
        return new self();
    }
}